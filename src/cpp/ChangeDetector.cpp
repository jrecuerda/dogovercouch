#include <ChangeDetector.h>
#include <iostream>
#include <pcl/octree/octree.h>
#include <chrono>

ChangeDetector::ChangeDetector(double resolution, int noise_filter)
	: _noise_filter(noise_filter)
	, _resolution(resolution)
{
}

pcl::PointCloud<PointType>::Ptr ChangeDetector::getDiff(pcl::PointCloud<PointType>::ConstPtr cloud1, pcl::PointCloud<PointType>::ConstPtr cloud2)
{
	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();
	auto octree = std::make_shared<pcl::octree::OctreePointCloudChangeDetector<PointType>>(_resolution);

	octree->setInputCloud(cloud1);
	octree->addPointsFromInputCloud();

	octree->switchBuffers();

	octree->setInputCloud(cloud2);
	octree->addPointsFromInputCloud();

	std::shared_ptr<std::vector<int> > newPointIdxVector = std::make_shared<std::vector<int>>();

	octree->getPointIndicesFromNewVoxels(*newPointIdxVector, _noise_filter);

	//std::cout << "Points = " << newPointIdxVector->size();//<< std::endl;

	pcl::PointCloud<PointType>::Ptr filtered_cloud(new pcl::PointCloud<PointType>());

	filtered_cloud->points.reserve(newPointIdxVector->size());

	for (std::vector<int>::iterator it = newPointIdxVector->begin(); it != newPointIdxVector->end(); ++it)
	{
		auto currPoint = cloud2->points[*it];
		if ( currPoint.z > 1.2 )
		{
			filtered_cloud->points.push_back(currPoint);
		}
	}

	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);
	//std::cout << "; Elapsed: " << elapsed.count() << std::endl;
	return filtered_cloud;
}
