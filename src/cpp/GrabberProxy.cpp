#include <GrabberProxy.h>
#include <kinect2_grabber.h>
#include <chrono>
#include <future>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread/future.hpp>
#include <boost/thread.hpp>

using namespace std::chrono_literals;

GrabberProxy::GrabberProxy(Callback callback, FinishCheckCallback finishChecker)
	: _callback(callback)
	, _finishChecker(finishChecker)
	, _running(false)
{
}

void GrabberProxy::run( LaunchMode mode )
{
	// Kinect2Grabber
	boost::shared_ptr<pcl::Grabber> grabber = boost::make_shared<pcl::Kinect2Grabber>();

	boost::signals2::connection c = grabber->registerCallback(_callback);

	auto future = boost::async( [grabber, c, this] 
	{
		grabber->start();
		_running = true;

		while (!_finishChecker())
		{
			//std::this_thread::sleep_for( std::chrono::milliseconds(100) );
			boost::this_thread::sleep(boost::posix_time::millisec(100));
		}

		grabber->stop();
		_running = false;
		
		//close connection
	});


	if (mode == LaunchMode::SYNC)
	{
		future.get();
	}
}

bool GrabberProxy::isRunning() const
{
	return _running;
}
