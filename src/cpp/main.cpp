// Disable Error C4996 that occur when using Boost.Signals2.
#ifdef _DEBUG
#define _SCL_SECURE_NO_WARNINGS
#endif

#include <GrabberProxy.h>
#include <ChangeDetector.h>

#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <boost/thread/mutex.hpp>
#include <qmediaplayer.h>
#include <qmediaplaylist.h>
#include <QApplication>

#include <chrono>
#include <iostream>
#include <atomic>

pcl::PointCloud<PointType>::ConstPtr referencePicker(int secondsToWait)
{
	pcl::PointCloud<PointType>::ConstPtr currentCloud;

    boost::mutex mutex;

	auto getCloud = [&currentCloud, &mutex](const pcl::PointCloud<PointType>::ConstPtr& cloud)
	{
		{
			boost::mutex::scoped_lock lock(mutex);
			currentCloud = cloud->makeShared();
		}
	};
	// Move to a object
	bool pickStartTime = true;
	std::chrono::system_clock::time_point start;
	auto finishChecker = [&pickStartTime, &start, &secondsToWait]() -> bool
	{
		if (pickStartTime)
		{
			start = std::chrono::system_clock::now();
			pickStartTime = false;
		}

		auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - start);
		return elapsed.count() > secondsToWait;
	};

	GrabberProxy grabber(getCloud, finishChecker);
	grabber.run(LaunchMode::SYNC);

	return currentCloud;
}

#include <pcl/segmentation/sac_segmentation.h>
// It has to be a XYZ type
auto planarSegmentation( pcl::PointCloud<PointType>::ConstPtr cloud)
{

	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
	// Create the segmentation object
	pcl::SACSegmentation<PointType> seg;
	// Optional
	seg.setOptimizeCoefficients(true);
	// Mandatory
	seg.setModelType(pcl::SACMODEL_PLANE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setDistanceThreshold(0.01);

	seg.setInputCloud(cloud);
	seg.segment(*inliers, *coefficients);

	if (inliers->indices.size() == 0)
	{
		PCL_ERROR("Could not estimate a planar model for the given dataset.");
	}

	std::cerr << "Model coefficients: " << coefficients->values[0] << " "
		<< coefficients->values[1] << " "
		<< coefficients->values[2] << " "
		<< coefficients->values[3] << std::endl;

	pcl::PointCloud<PointType>::Ptr ret(new pcl::PointCloud<PointType>());
	for (size_t i = 0; i < inliers->indices.size(); ++i)
	{
		ret->points.push_back(cloud->points[inliers->indices[i]]);
	}

	return ret;
	//std::cerr << "Model inliers: " << inliers->indices.size() << std::endl;
	//for (size_t i = 0; i < inliers->indices.size(); ++i)
	//	std::cerr << inliers->indices[i] << "    " << cloud->points[inliers->indices[i]].x << " "
	//	<< cloud->points[inliers->indices[i]].y << " "
	//	<< cloud->points[inliers->indices[i]].z << std::endl;
}

#include <pcl/filters/statistical_outlier_removal.h>
auto filterCloud(pcl::PointCloud<PointType>::ConstPtr cloud)
{
	if (cloud->points.size() < 1000)
	{
		return cloud->makeShared();
	}

	pcl::PointCloud<PointType>::Ptr cloud_filtered(new pcl::PointCloud<PointType>);

	// Create the filtering object
	pcl::StatisticalOutlierRemoval<PointType> sor;
	sor.setInputCloud(cloud);
	sor.setMeanK(50);
	sor.setStddevMulThresh(1.0);
	sor.filter(*cloud_filtered);

	return cloud_filtered;
}

void displayMainView(boost::mutex& mutex, pcl::PointCloud<PointType>::Ptr & cloud)
{
    // PCL Visualize
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(
        new pcl::visualization::PCLVisualizer( "DogOverCouch" ) );
    viewer->setCameraPosition( 0.0, 0.0, -2.5, 0.0, 0.0, 0.0 );
	// Visualization has to be done in main thread
	while(!viewer->wasStopped())
	{
		viewer->spinOnce();

		boost::mutex::scoped_try_lock lock(mutex);
		if (lock.owns_lock() && cloud) {
			// Update Point Cloud
			if (!viewer->updatePointCloud(cloud, "DogOverCouch")) 
			{
				viewer->addPointCloud(cloud, "DogOverCouch");
			}
		}
	}
}

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/local_time_adjustor.hpp>
#include <boost/date_time/c_local_time_adjustor.hpp>

class Alarm
{
public:
	Alarm()
		: _playList(std::make_shared<QMediaPlaylist>())
		, _player(std::make_shared<QMediaPlayer>())
		, _numDetections(0)
	{
		_playList->addMedia(QUrl::fromLocalFile("../../res/alarm.mp3"));
		_playList->setPlaybackMode(QMediaPlaylist::Loop);
		_player->setPlaylist(_playList.get());
		_player->setVolume(100);
	}

	void play()
	{
		int currDetections = _numDetections++;
		std::cout << getDateTime() << " - Play: Detections=" << currDetections << std::endl;
		if (currDetections > 2 && currDetections < 150)
		{
			boost::mutex::scoped_try_lock lock(_mutex);
			if (lock.owns_lock())
			{
				_player->play();
			}
		}
	}

	void stop()
	{
		if (_numDetections.load() > 0)
		{
			std::cout << getDateTime() << " - Stop" << std::endl;
			_numDetections = 0;
			_player->stop();
		}
	}

private:
	boost::mutex _mutex;
	std::atomic_int _numDetections;
	std::shared_ptr<QMediaPlaylist> _playList;
	std::shared_ptr<QMediaPlayer> _player;

	std::string getDateTime()
	{
		using boost::posix_time::ptime;
		using boost::posix_time::second_clock;
		using boost::posix_time::to_simple_string;
		using boost::gregorian::day_clock;
		ptime todayUtc(day_clock::universal_day(), second_clock::universal_time().time_of_day());
		return to_simple_string(todayUtc);
	}
};

auto detectChangesGrabber(boost::mutex& mutex, pcl::PointCloud<PointType>::ConstPtr reference, pcl::PointCloud<PointType>::Ptr& diffCloud, std::shared_ptr<Alarm> alarm)
{
	std::shared_ptr<ChangeDetector> detector = std::make_shared<ChangeDetector>(0.1, 7);

	auto processingPipeline = [detector, reference, &diffCloud, &mutex, alarm](const pcl::PointCloud<PointType>::ConstPtr& cloud)
	{
		{
			boost::mutex::scoped_lock lock(mutex);
			diffCloud = detector->getDiff(reference, cloud);
			if (diffCloud->points.size() > 2000)
			{
				std::cerr << "High number of points " << diffCloud->points.size() << std::endl;
				boost::async([alarm]() {alarm->play(); });
			}
			else
			{
				alarm->stop();
			}
		}
		//boost::this_thread::sleep(boost::posix_time::millisec(500));
		boost::this_thread::sleep(boost::posix_time::millisec(25000));
	};
	
	//Current grabber -> infinite
	std::shared_ptr<GrabberProxy> grabber = std::make_shared<GrabberProxy>(processingPipeline, []()->bool {return false; });
	return grabber;
}

auto detectAndShowWall(pcl::PointCloud<PointType>::ConstPtr inputCloud)
{
	// Detect the wall
	auto planarSeg =  planarSegmentation(inputCloud);
	auto viewerWall = std::make_shared<pcl::visualization::CloudViewer>("Wall");
	viewerWall->showCloud(planarSeg);
	return viewerWall;
}

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	//auto player = std::make_shared<QMediaPlayer>();
	//player->setMedia(QUrl::fromLocalFile("../../res/alarm.mp3"));
	//player->setVolume(50);
	//player->play();
	auto alarm = std::make_shared<Alarm>();

    boost::mutex mutex;
	pcl::PointCloud<PointType>::Ptr diffCloud;

	// Get reference cloud
	auto reference = referencePicker(2);

	// Detect and show wall
	//auto viewerWall = detectAndShowWall(reference);

	// Detect the changes -> Start the live grabber
	auto changesDetectorGrabber = detectChangesGrabber(mutex, reference, diffCloud, alarm);
	changesDetectorGrabber->run();

	// Display main view
	//displayMainView(mutex, diffCloud);

	return app.exec();
}
