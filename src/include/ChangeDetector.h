#pragma once

#include <definitions.h>

#include <pcl/point_cloud.h>

#include <memory>

class ChangeDetector
{
public:
	ChangeDetector(double resolution, int noise_filter);

	pcl::PointCloud<PointType>::Ptr getDiff(pcl::PointCloud<PointType>::ConstPtr cloud1, pcl::PointCloud<PointType>::ConstPtr cloud2);

private:

	int _noise_filter;
	double _resolution;
};