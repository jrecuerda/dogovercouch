#pragma once

#include <definitions.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <boost/function.hpp>

enum class LaunchMode
{
	ASYNC,
	SYNC
};

class GrabberProxy
{
public:
	using Callback = boost::function<void(const pcl::PointCloud<PointType>::ConstPtr&)>;
	using FinishCheckCallback = boost::function<bool()>;

	GrabberProxy( Callback callback, FinishCheckCallback finishChecker);

	void run( LaunchMode mode = LaunchMode::ASYNC);

	bool isRunning() const;

private:
	bool _running;
	Callback _callback;
	FinishCheckCallback _finishChecker;
};
